let $winner = null
,$turn = true
,$board = {}
,socket = io.connect('http://localhost:3000');


socket.on('chat.message',function(message){
	console.log(message)
	this.messages.push(message)
}.bind(this))

socket.on('chat.connection',function(data){
	
console.log(data)
	$board = data.board	
}.bind(this))	


function takeTurn(piece){
	if($board[piece.data('y')][piece.data('x')] === null){
		$board[piece.data('y')][piece.data('x')] = $turn
		piece.text($turn?'X':'O')

		checkForWin()

		if($winner !== null){
			announceWin()
		}

		$turn = !$turn
	}
}

function checkForWin(){
	if(
		$board[2][2]!== null && 
		(
			(	$board[1][1] === $board[2][2] && $board[2][2] === $board[3][3]) || 
			(	$board[3][1] === $board[2][2] && $board[2][2] === $board[1][3]))
		){
		$winner = $turn			
	}

	for (var row in $board) {      
 		if($board[1][row]!== null && $board[1][row] === $board[2][row] === $board[3][row]){
			$winner = $turn
 		}else{
			arr = Object.values($board[row])
			if(arr.every(v => v === $turn)){
				$winner = $turn
			}
 		}
 	}
}

function announceWin(){
	alert( ($winner?'X':'O') +' has one')
}


$(document).ready(function(e){
	$('.game-board-cell').on('click',function(e){
		// socket.emit('chat.nameChosen',{'x':,'y'})
		takeTurn($(this))
	})
})

