var express = require('express')
var app = express()
var server = require('http').Server(app)
var io = require('socket.io')(server)

let roomTemplate =  
{
	'turn' : 1,
	'complete' : false,
	'players' : {
		1:{
			'piece':'X',
			'name':'',
			'connected':false
		},
		2:{
			'piece':'O',
			'name':'',
			'connected':false
		}
	},
	'board' : {
		1:{
			1:null,
			2:null,
			3:null
		},
		2:{
			1:null,
			2:null,
			3:null
		},
		3:{
			1:null,
			2:null,
			3:null
		}
	}
},
rooms = [
	JSON.parse(JSON.stringify(roomTemplate))
]

server.listen(3000)

app.use(express.static(__dirname + '/public'))
app.get('/',function(request,response){
	response.sendFile(__dirname + '/index.html')
})

io.on('connection',function(socket){

	rooms.forEach(function(room,index) {
		console.log(room)
  		if((!room.players[1].connected || !room.players[2].connected) && !room.complete){
			socket.roomId = index
			socket.join(index)
			console.log('hit')
  		}
	})
			console.log(socket.roomId)

	if(!socket.roomId){
		rooms.push(JSON.parse(JSON.stringify(roomTemplate)))
		let index = rooms.length - 1
		socket.roomId = index
		socket.join(index)
	}


	let room = rooms[socket.roomId]

	socket.player = room.players[1].connected ? 2 : 1
	room.players[socket.player].connected = true

	socket.on('disconnect', function(){
		room.players[socket.player].connected = false
		io.in(socket.roomId).emit('board.disconnect',room.players[socket.player].name)
	})

 	socket.emit('board.connection',{
 		'board': room.board,
 		'player': room.players[socket.player],
 		'turn':  room.turn === socket.player,
 		'opponent': room.players[socket.player === 1 ? 2:1]
 	})

 	socket.on('board.playerName',function(name){
 		room.players[socket.player].name = name
 		socket.emit('board.opponentConnected',room.players[socket.player])
 	})


 	socket.emit('board.playerName',room.players[socket.player].name)

	socket.on('board.makeMove',function(coordinates){
			
		if(room.turn === socket.player && room.board[coordinates.y][coordinates.x] === null){
			room.board[coordinates.y][coordinates.x] = room.players[socket.player].piece
			
			io.in(socket.roomId).emit('board.movePlayed',room.board)

			if(room.complete = checkForWin()){
				io.in(socket.roomId).emit('board.winner',room.players[socket.player])
			}
			
			room.turn = room.turn === 1 ? 2 : 1
		}	
	})

	function checkForWin(){
		if(
			room.board[2][2]!== null && 
			(
				(	room.board[1][1] === room.board[2][2] && room.board[2][2] === room.board[3][3]) || 
				(	room.board[3][1] === room.board[2][2] && room.board[2][2] === room.board[1][3]))
		){
			return true	
		}

		for (var row in room.board) {    
	 		if(room.board[1][row]!== null && room.board[1][row] === room.board[2][row] && room.board[2][row] === room.board[3][row]){
								return true
	 		}else{
				arr = Object.values(room.board[row])
				if(arr.every(v => v !== null && v === arr[1])){
					return true
				}
	 		}
	 	}

	 	return false
	}
})
